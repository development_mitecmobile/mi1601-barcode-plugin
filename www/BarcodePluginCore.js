var exec = require('cordova/exec');

//constructor
function BarcodePluginCore(){
	
}

BarcodePluginCore.prototype.scanItem = function (successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "BarcodePluginCore", "performScan", []);
}

BarcodePluginCore.install = function(){
	if(!window.plugins){
		window.plugins = {};
	}
	window.plugins.barcodePluginCore = new BarcodePluginCore();
	return window.plugins.barcodePluginCore;
}

cordova.addConstructor(BarcodePluginCore.install);