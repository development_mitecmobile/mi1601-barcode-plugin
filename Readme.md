# Plugin MI1601

Plugin para Cordova para la funcionalidad de Barcode de la MI1601

### Instalación

```
cordova plugin add https://bitbucket.org/development_mitecmobile/mi1601-barcode-plugin.git
```


### Prerrequisitos

La versión 1D no requiere ningún permiso en especial.
La versión 2D requiere los permisos de Cámara y Almacenamiento externo.


```
    <platform name="android">
        <allow-intent href="market:*" />
		<config-file after="uses-permission" parent="/manifest" target="AndroidManifest.xml">
            <uses-permission android:name="android.permission.CAMERA" />
            <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
        </config-file>
    </platform>
```

### Utilización

El lector devuelve una cadena de texto con el código escaneado.
Del mismo modo, devuelve como una cadena el error detectado, incluído si se ha lanzado una lectura
que no ha conseguido leer un código.

```
	window.plugins.barcodePluginCore.scanItem(
		function(success){
			//Lógica en caso de éxito
		},
		function(error){
			//Lógica en caso de error
		}
	);	
```