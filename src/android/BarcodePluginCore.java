package cordova.plugin.mi1601;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.senter.support.openapi.StBarcodeScanner;
import java.io.UnsupportedEncodingException;
/**
 * This class echoes a string called from JavaScript.
 */
public class BarcodePluginCore extends CordovaPlugin {
	
	private CallbackContext callback;
    private boolean isScanning;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
		this.callback = callbackContext;
		switch(action){
			case "performScan":
			    this.scanItem();
				return true;
			default:
				return false;
		}
    }
	
	private void scanItem() {
        if (isScanning){
			callback.error("Ya hay un escaneo iniciado");
            return;
		}
        isScanning = true;
		try {
			StBarcodeScanner scanner = StBarcodeScanner.getInstance();
			StBarcodeScanner.BarcodeInfo rslt = scanner.scanBarcodeInfo();
			
			if(rslt == null){
				callback.error("Fallo de lectura");
			}else{
				callback.success(new String(rslt.getBarcodeValueAsBytes(),"utf-8"));
			}
			
		} catch (InterruptedException e) {
			callback.error("Escaneo interrumpido");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} finally {
			isScanning = false;
		}
    }
}
